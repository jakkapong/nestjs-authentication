import { Catch, ExceptionFilter, HttpException, ArgumentsHost, Logger, HttpStatus } from '@nestjs/common';
import { GqlArgumentsHost, GqlExceptionFilter } from '@nestjs/graphql';

@Catch()
export class HttpErrorFilter implements ExceptionFilter, GqlExceptionFilter {
    catch(exception: HttpException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const request = ctx.getRequest();
        const reponse = ctx.getResponse();
        const status = exception.getStatus ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;

        if (request) {
            // จัดรูปแบบการ Response
            const errorResponse = {
                code: status,
                timestamp: new Date().toLocaleDateString(),
                path: request.url,
                method: request.method,
                message: (status !== HttpStatus.INTERNAL_SERVER_ERROR) ? (exception.message.error || exception.message || null)
                    : 'Internal server error',
            };

            if (status === HttpStatus.INTERNAL_SERVER_ERROR) {
                Logger.error(
                    `${request.method} ${request.url}`,
                    exception.stack,
                    'ExceptionFilter',
                );
            } else {
                Logger.error(
                    `${request.method} ${request.url}`,
                    JSON.stringify(errorResponse),
                    'ExceptionFilter',
                );
            }

            reponse.status(status).json(errorResponse);

        } else {
            const gqlHost = GqlArgumentsHost.create(host);
            const info = gqlHost.getInfo();
            const errorResponse = {
                code: status,
                timestamp: new Date().toLocaleDateString(),
                message: (exception.message.error || exception.message)
            };
            Logger.error(
                `${info.parentType} ${info.fieldName}`,
                JSON.stringify(errorResponse),
                'ExceptionFilter',
            );

        }
    }

}