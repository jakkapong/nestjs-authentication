import { Table, Column, Model, CreatedAt, UpdatedAt, DataType, HasMany, IsUUID, PrimaryKey, BeforeCreate } from 'sequelize-typescript';
import { TokenModel } from '../token/token.model';

import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcryptjs';

@Table({
    tableName: 'user',
    comment: 'ข้อมูลผู้ใช้งาน'
})
export class UserModel extends Model<UserModel> {

    @IsUUID(4)
    @PrimaryKey
    @Column({
        defaultValue: DataType.UUIDV4
    })
    id: string;

    @Column({
        type: DataType.STRING(200),
        unique: true,
        comment: 'ชื่อผู้ใช้งาน เข้าระบบ'
    })
    username: string;

    @Column({
        type: DataType.STRING(200),
        // allowNull: false,
        // unique: true,
        comment: 'อีเมล์ผู้ใช้งาน'
    })
    email: string

    @Column({
        type: DataType.TEXT,
        comment: 'รหัสผ่าน'
    })
    password: string;

    @Column({
        type: DataType.STRING(200),
        comment: 'ขื่อจริง'
    })
    first_name: string

    @Column({
        type: DataType.STRING(200),
        comment: 'นามสกุล'
    })
    last_name: string

    @Column({
        type: DataType.STRING(200),
        comment: 'รหัสผู้บันทึกข้อมูล'
    })
    created_by: string;

    @Column({
        type: DataType.STRING(200),
        comment: 'รหัสผู้แก้ไขข้อมูล'
    })
    updated_by: string;

    @CreatedAt
    created_at: Date;

    @UpdatedAt
    updated_at: Date;

    @HasMany(() => TokenModel)
    tokens: TokenModel[];

    get refresh_token(): string {
        return this.getDataValue('refresh_token');
    }

    set refresh_token(token: string) {
        this.setDataValue('refresh_token', token);
    }

    @BeforeCreate
    static async hashPassword(instance: UserModel) {
        instance.password = await bcrypt.hash(instance.password, 10);
    }

    // ตรวจสอบรหัสผ่าน
    async comparePassword(attempt: string) {
        return await bcrypt.compare(attempt, this.password);
    }

    // รูปแบบการตอบกลับ
    toResponseObject(token: boolean = true) {
        const { id, username, first_name, last_name, created_at, refresh_token, tokens } = this;
        let reponseObject: any = { id, username, first_name, last_name, created_at, refresh_token, tokens };
        if (token) {
            reponseObject.access_token = this.access_token
        }
        return reponseObject;
    }

    // สร้าง token
    private get access_token() {
        const { id, username, first_name, last_name } = this;
        return jwt.sign(
            {
                id,
                username,
                first_name,
                last_name
            },
            process.env.SECRET,
            { expiresIn: 15 * 60 }
        )
    }

}