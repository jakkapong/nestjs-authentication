import { Controller, Post, Get, Body, UseGuards, UsePipes, HttpCode } from '@nestjs/common';
import { UserService } from './user.service';
import { UserRGT, UserDTO, UserRO } from './user.dto';
import { AuthGuard } from '../../shared/auth.guard';
import { ValidationPipe } from '../../shared/validation.pipe';
import { User } from './user.decorator';


@Controller()
export class UserController {

    constructor(private userService: UserService) { }

    @Get('api/users')
    @UseGuards(new AuthGuard())
    showAll(): Promise<UserRO[]> {
        return this.userService.showAll();
    }

    @Post('api/users/refresh-token')
    @HttpCode(200)
    @UsePipes(new ValidationPipe())
    refreshToken(@Body('refresh_token') token: string) {
        return this.userService.refreshToken(token);
    }

    @Post('login')
    @UsePipes(new ValidationPipe())
    login(@Body() data: UserDTO): Promise<UserRO> {
        return this.userService.login(data);
    }

    @Post('register')
    @UsePipes(new ValidationPipe())
    register(@Body() data: UserRGT): Promise<UserRO> {
        return this.userService.register(data);
    }

}
