import { UseGuards } from "@nestjs/common";
import { Resolver, Query } from "@nestjs/graphql";

import { AuthGuard } from "../../shared/auth.guard";
import { UserService } from "./user.service";
import { User } from "./user.decorator";

@Resolver('User')
export class UserResolver {

    constructor(private userService: UserService) { }

    @Query()
    @UseGuards(new AuthGuard())
    users(@User() user) {
        console.log('user: ', user);
        return this.userService.showAll();
    }

}