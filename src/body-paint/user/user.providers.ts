import { UserModel } from "./user.model";

export const userProviders = [
    {
        provide: 'USER_REPOSITORY',
        useValue: UserModel,
    }
];

