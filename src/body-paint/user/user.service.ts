import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';

import { UserModel } from './user.model';
import { TokenService } from '../token/token.service';
import { UserRGT, UserDTO, UserRO } from './user.dto';

@Injectable()
export class UserService {

    constructor(
        @Inject('USER_REPOSITORY')
        private readonly userRepository: typeof UserModel,
        private tokenService: TokenService,
    ) { }

    // แสดงผู้ใช้งาน
    async showAll(): Promise<UserRO[]> {
        const users = await this.userRepository.findAll<UserModel>();
        return users.map(user => user.toResponseObject(false));
    }

    // สร้างข้อมูล Access Token
    async refreshToken(token: string) {
        return await this.tokenService.verify(token);
    }

    // ล็อกอิน
    async login(data: UserDTO): Promise<UserRO> {
        const { username, password } = data;
        const user: any = await this.userRepository.findOne<UserModel>({ where: { username } });
        if (!user || !(await user.comparePassword(password))) {
            throw new HttpException('Invalid username/password', HttpStatus.BAD_REQUEST);
        }
        user.refresh_token = await this.tokenService.generate(user);
        return user.toResponseObject();
    }

    // สมัคสมาชิก
    async register(data: UserRGT): Promise<UserRO> {
        const { username } = data;
        let user = await this.userRepository.findOne<UserModel>({ where: { username } });
        if (user) {
            throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
        }
        user = await this.userRepository.create(data);
        return user.toResponseObject(false);
    }


}
