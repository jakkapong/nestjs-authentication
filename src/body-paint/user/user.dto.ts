import { IsNotEmpty, IsString } from "class-validator";

export class UserDTO {

    @IsNotEmpty()
    username: string;
    @IsNotEmpty()
    password: string;

}

export class UserRGT {
    @IsNotEmpty()
    @IsString()
    username: string;

    email?: string;

    @IsNotEmpty()
    @IsString()
    password: string;

    @IsNotEmpty()
    @IsString()
    first_name: string;

    @IsNotEmpty()
    @IsString()
    last_name: string;
}

export class UserRO {
    id: string;
    username: string;
    email?: string;
    first_name: string;
    last_name: string;
    created_at?: string;
    access_token?: string;
    refresh_token?: string;
}