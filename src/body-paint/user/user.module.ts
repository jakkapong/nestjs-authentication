import { Module } from '@nestjs/common';

import { UserController } from './user.controller';
import { UserService } from './user.service';
import { userProviders } from './user.providers';
import { tokenProviders } from '../token/token.providers';
import { TokenService } from '../token/token.service';
import { UserResolver } from './user.resolver';

@Module({
    controllers: [UserController],
    providers: [
        UserResolver,
        UserService,
        TokenService,
        ...userProviders,
        ...tokenProviders,
    ],
})
export class UserModule { }
