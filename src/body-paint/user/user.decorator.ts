import { createParamDecorator } from '@nestjs/common';

export const User = createParamDecorator((data, req) => {
    /** 
     * data = param->id ที่ส่งมาจาก @User('id')
     * req คือข้อมูลที่ส่งมา request client
    */
    return data ? req.user[data] : req.user;
});