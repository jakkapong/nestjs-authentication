import { Sequelize } from 'sequelize-typescript';
import { operatorsAliases }  from './operators-aliases.sequelize';

import { UserModel } from './user/user.model';
import { TokenModel } from './token/token.model';

export const databaseProviders = [
    {
        provide: 'SEQUELIZE',
        useFactory: async () => {
            const sequelize = new Sequelize({
                operatorsAliases,
                dialect: 'mysql',
                host: '10.0.0.120',
                port: 7001,
                username: 'root',
                password: '123456',
                database: 'body_paint',
                timezone: '+07:00',
            });
            sequelize.addModels([
                UserModel,
                TokenModel
            ]);
            // await sequelize.sync();
            return sequelize;
        },
    },
];