import { Module } from '@nestjs/common';

import { TokenController } from './token.controller';
import { TokenService } from './token.service';
import { tokenProviders } from './token.providers';

@Module({
    controllers: [TokenController],
    providers: [
        TokenService,
        ...tokenProviders
    ],
})
export class TokenModule { }
