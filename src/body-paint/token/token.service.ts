import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { TokenModel } from './token.model';
import { UserModel } from '../user/user.model';
import { UserDTO } from '../user/user.dto';

import * as moment from 'moment';

@Injectable()
export class TokenService {

    private periodOfToken: number = 30; // ค่าเวลาของ Refresh Token (days)

    constructor(
        @Inject('TOKEN_REPOSITORY')
        private readonly tokenRepository: typeof TokenModel,
    ) { }

    async generate(user: UserModel): Promise<string> {
        const refreshToken = await this.tokenRepository.create<TokenModel>({
            token_expires: moment().add(this.periodOfToken, 'days').format(),
            userId: user.id
        });
        return refreshToken.token;
    }

    async verify(token: string): Promise<UserDTO> {
        const refreshToken = await this.tokenRepository.findOne<TokenModel>({ where: { token }, include: ['user'] });
        if (!refreshToken) {
            throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
        }
        let user = refreshToken.user;
        user.refresh_token = refreshToken.token;
        return user.toResponseObject();

    }
}
