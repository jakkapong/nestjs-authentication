import { Table, Model, CreatedAt, Column, DataType, ForeignKey, BelongsTo, IsUUID } from "sequelize-typescript";

import { UserModel } from "../user/user.model";

@Table({
    tableName: 'token',
    updatedAt: false,
    comment: 'ตารางเก็บข้อมูล refresh token',
})
export class TokenModel extends Model<TokenModel> {

    @Column({
        type: DataType.INTEGER,
        autoIncrement: true,
        primaryKey: true
    })
    id: number;

    @CreatedAt
    created_at: Date;

    @Column({
        type: DataType.UUID,
        defaultValue: DataType.UUIDV4,
        // allowNull: false,
        // unique: true,
        comment: 'refresh token',
    })
    token: string;

    @Column({
        type: DataType.DATE,
        allowNull: false,
        comment: 'วันหมดอายุ refresh token',
    })
    token_expires: Date

    @ForeignKey(() => UserModel)
    @IsUUID(4)
    @Column
    userId: string;

    @BelongsTo(() => UserModel)
    user: UserModel;

}