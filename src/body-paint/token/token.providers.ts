import { TokenModel } from "./token.model";

export const tokenProviders = [
    {
        provide: 'TOKEN_REPOSITORY',
        useValue: TokenModel,
    }
];

